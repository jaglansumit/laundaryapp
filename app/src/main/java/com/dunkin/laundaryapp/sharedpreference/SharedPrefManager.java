package com.dunkin.laundaryapp.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;

import com.dunkin.laundaryapp.activities.User;

public class SharedPrefManager {

    public static final String SHARED_PREF_NAME ="my_shared_preff";

    private static SharedPrefManager mInstance;
    private Context mCtx;

    private SharedPrefManager(Context mCtx){
        this.mCtx = mCtx;
    }

    public static synchronized SharedPrefManager getInstance(Context mCtx){
        if(mInstance==null){
            mInstance = new SharedPrefManager(mCtx);
        }
        return mInstance;
    }

    public void saveUser(User user){

        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString("token",user.getToken());
        editor.putInt("id",user.getId());
        editor.putString("name",user.getName());
        editor.putString("address",user.getAddress());
        editor.putInt("role_id",user.getRole_id());
        editor.putString("phone",user.getPhone());
        editor.apply();
    }

    public boolean isLoggedIn(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getInt("id",-1) != -1;

    }

  public User getUser(){
      SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
      return new User(
              sharedPreferences.getString("token",null),
              sharedPreferences.getInt("id",-1),
              sharedPreferences.getString("name",null),
              sharedPreferences.getString("address",null),
              sharedPreferences.getInt("role_id",-1),
              sharedPreferences.getString("phone",null)
      );
  }


    public void clear(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
