package com.dunkin.laundaryapp;

import java.util.List;

public class OrdersDefaultResponse {
    private List<OrdersList> orders;

    public OrdersDefaultResponse(List<OrdersList> orders) {
        this.orders = orders;
    }

    public List<OrdersList> getOrders() {
        return orders;
    }
}
