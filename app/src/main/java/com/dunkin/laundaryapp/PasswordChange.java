package com.dunkin.laundaryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dunkin.laundaryapp.activities.Home;
import com.dunkin.laundaryapp.activities.User;
import com.dunkin.laundaryapp.retrofit.RetrofitClient;
import com.dunkin.laundaryapp.sharedpreference.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PasswordChange extends AppCompatActivity {
    EditText old,now,cnow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);

        old = findViewById(R.id.oldpassword);
        now = findViewById(R.id.newpassword);
        cnow = findViewById(R.id.cc_password);
        Button btn = findViewById(R.id.changepass);

        User user = SharedPrefManager.getInstance(this).getUser();
        String alonet = user.getToken();
        final String token = "Bearer "+alonet;

        final ProgressDialog progressDialog = new ProgressDialog(PasswordChange.this);
        progressDialog.setMessage("Address is Changing..");


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String oldpass = old.getText().toString();
                String newpass = now.getText().toString();
                String c_password = cnow.getText().toString();




                if(!newpass.equals(c_password) || oldpass.isEmpty() || newpass.isEmpty() || c_password.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Check your fields",Toast.LENGTH_SHORT).show();
                }
                else {
                    progressDialog.show();
                    Call<DefaultResponse> call = RetrofitClient.getInstance().getApi().passwordchange(oldpass,newpass,token);
                    call.enqueue(new Callback<DefaultResponse>() {
                        @Override
                        public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                            progressDialog.dismiss();
                            DefaultResponse defaultResponse = response.body();
                            String msg = defaultResponse.getMessage();
                            if(msg.equals("Changed")){
                                Intent sms = new Intent(getApplicationContext(), Home.class);
                                startActivity(sms);
                            }
                            else {
                                Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT);
                            }
                        }

                        @Override
                        public void onFailure(Call<DefaultResponse> call, Throwable t) {

                        }
                    });
                }
            }
        });


    }
}
