package com.dunkin.laundaryapp;

import java.util.List;

public class ServiceDefaultResponse {
    private List<ServicesList> services;

    public ServiceDefaultResponse(List<ServicesList> services) {
        this.services = services;
    }

    public List<ServicesList> getServices() {
        return services;
    }
}
