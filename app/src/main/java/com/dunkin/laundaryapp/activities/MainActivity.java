package com.dunkin.laundaryapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dunkin.laundaryapp.DashboardAdmin;
import com.dunkin.laundaryapp.DefaultResponse;
import com.dunkin.laundaryapp.PasswordChange;
import com.dunkin.laundaryapp.R;
import com.dunkin.laundaryapp.retrofit.RetrofitClient;
import com.dunkin.laundaryapp.sharedpreference.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText username , Epassword;
    Button login_btn;
    TextView signup_btn;
    int one = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}
        setContentView(R.layout.activity_main);

        findViewById(R.id.signup_page).setOnClickListener(this);
        findViewById(R.id.login).setOnClickListener(this);
        username = findViewById(R.id.email);
        Epassword = findViewById(R.id.password);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(SharedPrefManager.getInstance(this).isLoggedIn()) {

            User user = SharedPrefManager.getInstance(getApplicationContext()).getUser();
            int ids = user.getRole_id();
            int cmp = Double.compare(ids, one);

            if (cmp == 0) {

                Intent intent = new Intent(getApplicationContext(), DashboardAdmin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
            else {
                Intent intento = new Intent(getApplicationContext(), Home.class);
                intento.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intento);
            }

        }
    }

    public void userLogin(){

        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Please wait for your code!");
        progressDialog.show();


        String email = username.getText().toString().trim();
        String password = Epassword.getText().toString().trim();





        Call<DefaultResponse> call= RetrofitClient.getInstance().getApi()
                .userLogin(email , password);
        call.enqueue(new Callback<DefaultResponse>() {
            @Override
            public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {

progressDialog.dismiss();


               DefaultResponse defaultResponse = response.body();
               int role_id = defaultResponse.getRole();
               int retval = Double.compare(role_id,one);


                if(!defaultResponse.isError()) {

                    if (retval==0) {
                        SharedPrefManager.getInstance(MainActivity.this).saveUser(defaultResponse.getUser());
                        Intent intent = new Intent(getApplicationContext(), DashboardAdmin.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {

                        SharedPrefManager.getInstance(MainActivity.this).saveUser(defaultResponse.getUser());
                        Intent intentt = new Intent(getApplicationContext(), Home.class);
                        intentt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intentt);

                    }
                }

                 else {
                    Toast.makeText(getApplicationContext(),defaultResponse.getMessage(),Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<DefaultResponse> call, Throwable t) {

            }
        });

    }


    public void userSignup(){
        Intent signup = new Intent(getApplicationContext(), Signup.class);
        startActivity(signup);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login:
                userLogin();
            break;

            case R.id.signup_page:
                userSignup();
            break;
        }
    }
}
