package com.dunkin.laundaryapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dunkin.laundaryapp.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SelectDate extends AppCompatActivity {

    ToggleButton t1,t2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_date);

        t1 = findViewById(R.id.toggle_time1);
        t2 = findViewById(R.id.toggle_time);


        Intent i = getIntent();
        final String serice = i.getStringExtra("service");



        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date dayaftertomorrow = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date dayaftertomorrowtwo = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");

        String todayAsString = dateFormat.format(today);
        String tomorrowAsString = dateFormat.format(tomorrow);
        String dayafter = dateFormat.format(dayaftertomorrow);
        String twodayslater = dateFormat.format(dayaftertomorrowtwo);



        final Spinner spinner = (Spinner) findViewById(R.id.pickupdatespn);
        List<String> categories = new ArrayList<String>();
        categories.add(todayAsString);
        categories.add(tomorrowAsString);
        categories.add(dayafter);

        final Spinner spinner2 = (Spinner) findViewById(R.id.deliverydatespn);
        List<String> categories2 = new ArrayList<String>();
        categories2.add(tomorrowAsString);
        categories2.add(dayafter);
        categories2.add(twodayslater);




        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(dataAdapter2);


        Button btn = findViewById(R.id.nextbtn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String p_day= spinner.getSelectedItem().toString();
               String d_day= spinner2.getSelectedItem().toString();

               String p_time = (String) t1.getText();
               String d_time = (String) t2.getText();

               String p_all = p_day +" " + p_time;
               String d_all = d_day +" " + d_time;


                Intent ij = new Intent(getApplicationContext(),AddressEnter.class);
                ij.putExtra("service",serice);
                ij.putExtra("abc",p_all);
                ij.putExtra("def",d_all);
                startActivity(ij);
            }

        });


    }

}
