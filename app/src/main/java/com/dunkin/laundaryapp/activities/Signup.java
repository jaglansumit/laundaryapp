package com.dunkin.laundaryapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dunkin.laundaryapp.DefaultResponse;
import com.dunkin.laundaryapp.R;
import com.dunkin.laundaryapp.Signup2;
import com.dunkin.laundaryapp.VerificationCode;
import com.dunkin.laundaryapp.retrofit.RetrofitClient;

import java.util.Random;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup extends AppCompatActivity {

    private EditText Ename,Eemail,Epassword,Ec_password;
    private Button signup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}
        setContentView(R.layout.signup_activity);

        Ename= findViewById(R.id.name);
        Eemail= findViewById(R.id.email);
        Epassword =  findViewById(R.id.password);
        Ec_password = findViewById(R.id.c_password);
        signup = findViewById(R.id.signup);



        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });


    }

    private void registerUser(){
        final String name = Ename.getText().toString().trim();
        final String email = Eemail.getText().toString().trim();
        final String password = Epassword.getText().toString();
        String c_password = Ec_password.getText().toString().trim();



        if(name.isEmpty() || email.isEmpty() || password.isEmpty() || c_password.isEmpty() || !password.equals(c_password)){
            Toast.makeText(getApplicationContext(),"Fill all field!",Toast.LENGTH_SHORT).show();
        }

        else {


            final ProgressDialog progressDialog = new ProgressDialog(Signup.this);
            progressDialog.setMessage("Please wait for your code!");
            progressDialog.show();


            Random r = new Random( System.currentTimeMillis() );
            final int rnd= ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));



            Call<DefaultResponse> call = RetrofitClient.getInstance().getApi().getcode(rnd,email);

            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {

                    DefaultResponse defaultResponse = response.body();

                    String msg = defaultResponse.getMessage();

                    if(msg.equals("User Already Exists")) {

                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();

                    }
                    else {

                        progressDialog.dismiss();

                        Intent i = new Intent(getApplicationContext(), VerificationCode.class);
                        i.putExtra("code", rnd);
                        i.putExtra("uname", name);
                        i.putExtra("uemail", email);
                        i.putExtra("upassword", password);

                        startActivity(i);


                    }

                    }


                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {

                }
            });


        }
    }
}
