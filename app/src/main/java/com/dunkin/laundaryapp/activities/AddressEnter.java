package com.dunkin.laundaryapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dunkin.laundaryapp.DefaultResponse;
import com.dunkin.laundaryapp.R;
import com.dunkin.laundaryapp.retrofit.RetrofitClient;
import com.dunkin.laundaryapp.sharedpreference.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressEnter extends AppCompatActivity {

    TextView name,address,city,phonee,edit;
    Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        Intent i = getIntent();
        final String service= i.getStringExtra("service");
        final String p_time = i.getStringExtra("abc");
        final String d_time = i.getStringExtra("def");

        User user = SharedPrefManager.getInstance(this).getUser();
        String username = user.getName();
        final String useraddress  = user.getAddress();
        String phone = user.getPhone();
        String alonet = user.getToken();
        final String token = "Bearer "+alonet;


        name= findViewById(R.id.namecol);
        address= findViewById(R.id.addresscol);
        city=findViewById(R.id.citycol);
        b1 = findViewById(R.id.google);
        phonee = findViewById(R.id.phonecol);
        edit = findViewById(R.id.edit);


        name.setText(username);
        address.setText(useraddress);
        phonee.setText(phone);

        final ProgressDialog progressDialog = new ProgressDialog(AddressEnter.this);
        progressDialog.setMessage("Submitting your Order..");



        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressDialog.show();

                Call<DefaultResponse> call = RetrofitClient.getInstance().getApi().getstatus(d_time,p_time,service,useraddress,token);
                call.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        progressDialog.dismiss();
                        DefaultResponse defaultResponse = response.body();
                        Toast.makeText(getApplicationContext(),defaultResponse.getMessage(),Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Error Generated.. Try again",Toast.LENGTH_LONG).show();
                    }
                });



            }
        });

    }

}
