package com.dunkin.laundaryapp.activities;

import com.google.gson.annotations.SerializedName;

public class User {


    private String token;

    private int id;

    private String name;

    private String address;


    private  int role_id;

    private String phone;



    public User(String token, int id, String name, String address, int role_id , String phone) {
        this.token = token;
        this.id = id;
        this.name = name;
        this.address = address;
        this.role_id=role_id;
        this.phone=phone;

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getPhone() { return phone; }

    public void setPhone(String phone) { this.phone = phone; }

}

