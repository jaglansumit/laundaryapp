package com.dunkin.laundaryapp.retrofit;

import com.dunkin.laundaryapp.DefaultResponse;
import com.dunkin.laundaryapp.OrdersDefaultResponse;
import com.dunkin.laundaryapp.ServiceDefaultResponse;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface
{


  @FormUrlEncoded
   @POST("login")
  Call<DefaultResponse> userLogin(
    @Field("email") String email,
    @Field("password") String password
  );

  @POST("register")
  Call<DefaultResponse> registerUser(@Query("name") String name, @Query("email") String email, @Query("password") String password,
               @Query("c_password") String c_password,@Query("phone") String phone,@Query("address") String address);


  @GET("myorders")
    Call<OrdersDefaultResponse> getAllOrders(@Header("Authorization") String authtoken);

  @GET("allorders")
  Call<OrdersDefaultResponse> getAdminOrders(@Header("Authorization") String authtoken);



  @GET("services")
    Call<ServiceDefaultResponse> getAllServices(@Header("Authorization") String authtoken);

  @POST("placeorder")
    Call<DefaultResponse> getstatus ( @Query("deliverydate") String d_time ,
           @Query("pickupdate") String p_time,@Query("service_id") String service,@Query("address") String useraddress,@Header("Authorization") String authtoken);

  @POST("sendemail")
  Call<DefaultResponse> getcode (@Query("code") int code, @Query("email") String email);

  @POST("changeaddress")
  Call<DefaultResponse> change (@Query("address") String address, @Header("Authorization") String authtoken);

  @POST("passwordchange")
  Call<DefaultResponse> passwordchange (@Query("oldpass") String oldpass, @Query("newpass") String newpass,@Header("Authorization") String authtoken);





}
