package com.dunkin.laundaryapp;

import com.dunkin.laundaryapp.activities.User;

public class DefaultResponse {
    private boolean error;
    private String message;
    private int role;

    private User user;

    public DefaultResponse(boolean error, String message, User user, int role) {
        this.error = error;
        this.message = message;
        this.user = user;
        this.role = role;
    }

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }

    public boolean isError() {
        return error;
    }

    public int getRole() { return role; }
}
