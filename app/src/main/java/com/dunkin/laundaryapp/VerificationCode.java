package com.dunkin.laundaryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class VerificationCode extends AppCompatActivity {

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_code);

        Intent i = getIntent();
        final int rnd = i.getIntExtra("code",0);
        final String name = i.getStringExtra("uname");
        final String email = i.getStringExtra("uemail");
        final String password = i.getStringExtra("upassword");

        editText = findViewById(R.id.c_code);
        Button button = findViewById(R.id.code_btn);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int get = Integer.parseInt(editText.getText().toString());

                int retval = Double.compare(get, rnd);


                if(retval == 0){

                    Toast.makeText(getApplicationContext(),"Successfully Placed",Toast.LENGTH_SHORT).show();
                    Intent d = new Intent(getApplicationContext(),Signup2.class);
                    d.putExtra("uname",name);
                    d.putExtra("uemail",email);
                    d.putExtra("upassword",password);
                    startActivity(d);
                }

                else {
                    Toast.makeText(getApplicationContext(),"Please Enter Correct Code",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
