package com.dunkin.laundaryapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dunkin.laundaryapp.OrdersList;
import com.dunkin.laundaryapp.R;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.OrdersViewHolder> {

    private Context mCtx;
    private List<OrdersList> orders;

    public OrdersAdapter(Context mCtx, List<OrdersList> orders) {
        this.mCtx = mCtx;
        this.orders = orders;
    }


    @NonNull
    @Override
    public OrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.card_row,parent,false);
        return new OrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersViewHolder holder, int position) {
         OrdersList orderss= orders.get(position);

         holder.orderid.setText(orderss.getOrderid());
         holder.status.setText(orderss.getStatus());
         holder.bill.setText(orderss.getBill());
         holder.deliverdate.setText(orderss.getDeliverydate());
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    class OrdersViewHolder extends RecyclerView.ViewHolder{

        TextView orderid,status,bill,deliverdate;

        public OrdersViewHolder(@NonNull View itemView) {
            super(itemView);

            orderid =itemView.findViewById(R.id.ordernumber);
            status =itemView.findViewById(R.id.status);
            bill =itemView.findViewById(R.id.bill);
            deliverdate =itemView.findViewById(R.id.delivery_date);
        }
    }
}
