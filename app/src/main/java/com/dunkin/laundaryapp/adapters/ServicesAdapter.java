package com.dunkin.laundaryapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dunkin.laundaryapp.OrdersList;
import com.dunkin.laundaryapp.R;
import com.dunkin.laundaryapp.ServicesList;
import com.dunkin.laundaryapp.activities.SelectDate;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;
import java.util.zip.Inflater;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ServicesViewHolder> {

    private Context mctxx;
    private List<ServicesList> services;

    public ServicesAdapter(Context mctxx,List<ServicesList> services) {
        this.mctxx=mctxx;
        this.services=services;
    }

    @NonNull
    @Override
    public ServicesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mctxx).inflate(R.layout.service_card,parent,false);
        return new ServicesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServicesViewHolder holder, int position) {
        final ServicesList servicess = services.get(position);
        holder.serviceid.setText(servicess.getId());
        holder.titleservice.setText(servicess.getName());
        Picasso.with(mctxx).load(servicess.getImage()).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mctxx, SelectDate.class);
                i.putExtra("service",servicess.getName());
                mctxx.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    public class ServicesViewHolder extends RecyclerView.ViewHolder {

        TextView serviceid,titleservice;
        ImageView imageView;

        public ServicesViewHolder(@NonNull View itemView) {
            super(itemView);

            serviceid = itemView.findViewById(R.id.idservice);
            titleservice = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.images);
        }
    }
}
