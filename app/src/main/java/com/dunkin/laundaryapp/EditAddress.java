package com.dunkin.laundaryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dunkin.laundaryapp.activities.Home;
import com.dunkin.laundaryapp.activities.Signup;
import com.dunkin.laundaryapp.activities.User;
import com.dunkin.laundaryapp.retrofit.RetrofitClient;
import com.dunkin.laundaryapp.sharedpreference.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditAddress extends AppCompatActivity {

    EditText build , ara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);

        User user = SharedPrefManager.getInstance(this).getUser();
        String alonet = user.getToken();
        final String token = "Bearer "+alonet;

        build = findViewById(R.id.building);
        ara = findViewById(R.id.area);
        Button bt= findViewById(R.id.smt);

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String building = build.getText().toString();
                String area = ara.getText().toString();
                String address = building + area;

                final ProgressDialog progressDialog = new ProgressDialog(EditAddress.this);
                progressDialog.setMessage("Address is Changing..");
                progressDialog.show();

                Call<DefaultResponse> call = RetrofitClient.getInstance().getApi().change(address,token);
                call.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                        progressDialog.dismiss();
                        DefaultResponse defaultResponse = response.body();
                        Toast.makeText(getApplicationContext(),defaultResponse.getMessage(),Toast.LENGTH_SHORT).show();
                        Intent ij=new Intent(getApplicationContext(), Home.class);
                        startActivity(ij);
                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Try Again!",Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });
    }
}
