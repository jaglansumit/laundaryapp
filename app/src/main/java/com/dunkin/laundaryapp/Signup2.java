package com.dunkin.laundaryapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dunkin.laundaryapp.activities.Home;
import com.dunkin.laundaryapp.activities.MainActivity;
import com.dunkin.laundaryapp.retrofit.RetrofitClient;
import com.dunkin.laundaryapp.sharedpreference.SharedPrefManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup2 extends AppCompatActivity {

    EditText E_phone,E_address;
    Button signupp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}
        setContentView(R.layout.activity_signup2);

        Intent d = getIntent();
        final String name = d.getStringExtra("uname");
        final String email = d.getStringExtra("uemail");
        final String password = d.getStringExtra("upassword");

        final String c_password = password;

        E_phone = findViewById(R.id.u_phone);
        E_address = findViewById(R.id.u_address);
        signupp = findViewById(R.id.signupf);

        signupp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = E_phone.getText().toString();
                String address = E_address.getText().toString();
                Call<DefaultResponse> call = RetrofitClient.getInstance().getApi().registerUser(name,email,password,c_password,phone,address);

                call.enqueue(new Callback<DefaultResponse>() {
                    @Override
                    public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {

                        DefaultResponse defaultResponse = response.body();
                        if (!defaultResponse.isError()) {

                            SharedPrefManager.getInstance(Signup2.this).saveUser(defaultResponse.getUser());
                            Intent intent = new Intent(getApplicationContext(), Home.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                    }

                    @Override
                    public void onFailure(Call<DefaultResponse> call, Throwable t) {

                    }
                });
            }
        });


    }
}
