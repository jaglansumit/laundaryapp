package com.dunkin.laundaryapp.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dunkin.laundaryapp.OrdersDefaultResponse;
import com.dunkin.laundaryapp.OrdersList;
import com.dunkin.laundaryapp.R;
import com.dunkin.laundaryapp.ServiceDefaultResponse;
import com.dunkin.laundaryapp.ServicesList;
import com.dunkin.laundaryapp.adapters.ServicesAdapter;
import com.dunkin.laundaryapp.retrofit.RetrofitClient;
import com.dunkin.laundaryapp.sharedpreference.SharedPrefManager;
import com.dunkin.laundaryapp.activities.User;
import com.dunkin.laundaryapp.activities.SelectDate;
import com.dunkin.laundaryapp.adapters.OrdersAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {
    RecyclerView recyclerView, recyclerViewServices;
    public  String washing="washing";
    private List<OrdersList> orders;
    private List<ServicesList> services;
    private OrdersAdapter adapter;
    private ServicesAdapter servicesAdapter;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_home,container,false);

        TextView textView = (TextView) root.findViewById(R.id.orders);


        User user = SharedPrefManager.getInstance(getContext()).getUser();
        String alonet = user.getToken();
        String token = "Bearer "+alonet;

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        User user = SharedPrefManager.getInstance(getContext()).getUser();
        String alonet = user.getToken();
        String token = "Bearer "+alonet;


        recyclerView = view.findViewById(R.id.recyclerorderlist);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);

        recyclerViewServices=view.findViewById(R.id.sericerecylerview);
        recyclerViewServices.setLayoutManager(layoutManager);
        recyclerViewServices.setItemAnimator(new DefaultItemAnimator());


        Call<ServiceDefaultResponse> calltwo = RetrofitClient.getInstance().getApi().getAllServices(token);
        calltwo.enqueue(new Callback<ServiceDefaultResponse>() {
            @Override
            public void onResponse(Call<ServiceDefaultResponse> call, Response<ServiceDefaultResponse> response) {
                services = response.body().getServices();
                Log.i("bosss", String.valueOf(services));
                servicesAdapter = new ServicesAdapter(getActivity(),services);
                recyclerViewServices.setAdapter(servicesAdapter);
            }

            @Override
            public void onFailure(Call<ServiceDefaultResponse> call, Throwable t) {

            }
        });

        Call<OrdersDefaultResponse> call = RetrofitClient.getInstance().getApi().getAllOrders(token);

        call.enqueue(new Callback<OrdersDefaultResponse>() {
            @Override
            public void onResponse(Call<OrdersDefaultResponse> call, Response<OrdersDefaultResponse> response) {
                 orders = response.body().getOrders();
                 Log.i("nowww", String.valueOf(orders));
                 adapter = new OrdersAdapter(getActivity(),orders);
                 recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<OrdersDefaultResponse> call, Throwable t) {

            }
        });

    }
}
