package com.dunkin.laundaryapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.dunkin.laundaryapp.EditAddress;
import com.dunkin.laundaryapp.PasswordChange;
import com.dunkin.laundaryapp.R;
import com.dunkin.laundaryapp.sharedpreference.SharedPrefManager;
import com.dunkin.laundaryapp.activities.MainActivity;

public class SettingsFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_settings,container,false);
        TextView logout = view.findViewById(R.id.logout);
        TextView chg = view.findViewById(R.id.changeaddresse);
        TextView privacy = view.findViewById(R.id.privacy);

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent heya = new Intent(getActivity(), PasswordChange.class);
                startActivity(heya);
            }
        });

        chg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent hey = new Intent(getActivity(), EditAddress.class);
                startActivity(hey);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPrefManager.getInstance(getContext()).clear();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        return view;
    }
}
