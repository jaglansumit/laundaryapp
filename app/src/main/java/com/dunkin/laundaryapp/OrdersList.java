package com.dunkin.laundaryapp;

import com.google.gson.annotations.SerializedName;

public class OrdersList {
     @SerializedName("id")
    public String orderid;
    public String bill;
    public String status;
    public String deliverydate;
    private String address;
    private String phone;



    public OrdersList(String orderid, String bill, String status, String deliverydate ,String address,String phone) {
        this.orderid = orderid;
        this.bill = bill;
        this.status = status;
        this.deliverydate = deliverydate;
        this.address = address;
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getBill() {
        return bill;
    }

    public void setBill(String bill) {
        this.bill = bill;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(String deliverydate) {
        this.deliverydate = deliverydate;
    }
}
