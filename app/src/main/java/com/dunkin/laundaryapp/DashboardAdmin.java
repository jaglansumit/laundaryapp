package com.dunkin.laundaryapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.dunkin.laundaryapp.activities.MainActivity;
import com.dunkin.laundaryapp.activities.User;
import com.dunkin.laundaryapp.adapters.AdminOrdersAdapter;
import com.dunkin.laundaryapp.retrofit.RetrofitClient;
import com.dunkin.laundaryapp.sharedpreference.SharedPrefManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardAdmin extends AppCompatActivity {

    Button button;
    private List<OrdersList> orders;
    private AdminOrdersAdapter adapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_admin);

        User user = SharedPrefManager.getInstance(getApplicationContext()).getUser();
        String alonet = user.getToken();
        String token = "Bearer "+alonet;

        button = findViewById(R.id.adminout);
        recyclerView = findViewById(R.id.adminrecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));



        Call<OrdersDefaultResponse> call = RetrofitClient.getInstance().getApi().getAdminOrders(token);
        call.enqueue(new Callback<OrdersDefaultResponse>() {
            @Override
            public void onResponse(Call<OrdersDefaultResponse> call, Response<OrdersDefaultResponse> response) {

                orders = response.body().getOrders();
                adapter = new AdminOrdersAdapter(getApplicationContext(),orders);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<OrdersDefaultResponse> call, Throwable t) {

            }
        });



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPrefManager.getInstance(getApplicationContext()).clear();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
